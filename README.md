# AVASUS

Projeto da Fase 2 da LAIS/UFRN edital [N° 28/2023](https://lais.huol.ufrn.br/lais-ufrn-seleciona-alunos-de-graduacao-e-pos-graduacao-para-atuar-no-projeto-revela/)

## Como utilizar este repositório

É necessário que esteja instalado em sua máquina o [`node`](https://nodejs.org) e o [`pnpm`](https://pnpm.io).

Dentro do diretório do projeto, execute os seguintes comandos visualizar o projeto:

```bash
pnpm install
pnpm run dev
```

## Está lento para acessar os dados?

A API está hospedada gratuitamente na plataforma [Render](https://render.com/).

O serviço gratuito hiberna o servidor se este não estiver em uso constante,
por isso o primeiro acesso pode ser lento para requisitar os dados da API.
