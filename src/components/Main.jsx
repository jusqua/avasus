function Main({ children }) {
  return (
    <main className="w-full lg:w-[1024px] p-4 flex flex-col">{children}</main>
  );
}

export default Main;
